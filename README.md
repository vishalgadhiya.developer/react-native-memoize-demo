
# React Native Memoize Example / Demo

### MEMO:
“memo” comes from memoization, which is basically a form of optimization used mainly to speed up code by storing the results of expensive function calls and returning the stored result whenever the same expensive function is called again.

Memoization is a technique for executing a function once, usually a pure function, and then saving the result in memory. If we try to execute that function again, with the same arguments as before, it will just return the previously saved result from the first function’s execution, without executing the function again.

The purpose of the Memo is to skip unnecessary rerendering.

#### memo():
 - Wrap the child component with memo, which is skip re-rendring that component
#### useMemo():
 - useMemo Hook returns a memoized value.
#### useCallback():
 - useCallback Hook returns a memoized callback function.

#### NOTE: 
 - Above 3 method used in demo with explainnation please check App.js file.
 - Demo tested in ios simulator
 - Must see the terminal log, while check the demo so you will get the clear idea about memo and how skip reendering.

#### SCREENSHOT:
![01](/screenshots/ss1.png?raw=true "")
![02](/screenshots/ss2.png?raw=true "Click on title header to change color without reender list")
![03](/screenshots/ss2.png?raw=true "Clcik on product item name to set as hearder title without rerender all product item / list")
![04](/screenshots/ss2.png?raw=true "Clcik on product item price to set in hearder with multiplication without rerender all product item / list")


REFFRENCE:
https://www.youtube.com/watch?v=DXhcbMvNX04