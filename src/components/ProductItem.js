import React,{memo} from 'react';
import {View, Text, Image, Touchable, TouchableOpacity} from 'react-native';

const ProductItem = ({item, setTitle, setPrice}) => {
  console.log('Render product item');
  return (
    <View
      style={{
        width: '46%',
        marginHorizontal: 7,
        marginVertical: 7,
        height: 280,
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 15,
      }}>
      <Image
        style={{
          width: '100%',
          height: 200,
          borderWidth: 1,
          borderTopLeftRadius: 15,
          borderTopRightRadius: 15,
        }}
        source={{uri: item.thumbnail}}
      />
   
      <Text
        onPress={()=> setTitle(item.title)}
        style={{margin: 7, color: 'white', fontSize: 18, fontWeight: 500}}
        numberOfLines={1}>    
        {item.title}
      </Text>
   
      <Text onPress={()=> setPrice(item.price)} style={{margin: 7, color: 'green', fontSize: 20, fontWeight: 500}}>
        {item.price} $
      </Text>
    </View>
  );
};
export default memo(ProductItem);

/*

Memo():
Memo is the higher order component, Memo used for avoid rerending of the components.
Wrap the child component with memo to avoid rerendering of ProductItem,
ProductItem re-render only if the props will change.

*/
