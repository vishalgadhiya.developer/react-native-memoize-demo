/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useCallback, useMemo, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import ProductItem from './components/ProductItem';
import PRODUCT_JSON from './constant';

const App = () => {
  const [title, setTitle] = useState('Memoize Demo');
  const [titleColor, setTitleColor] = useState(false);
  const [price, setPrice] = useState(0);

  /**
   * In normal case, once you change the title color(using state),
   * it will rerender all the product item again which is bad things,
   * because we are nothing to do with product item.
   * Solution: Use memo()(Check "ProductItem.js" component)
   */

  const changeTitleColor = () => {
    setTitleColor(!titleColor);
  };

  /**
   * setProducTitle : is normal function.
   * Once the product title is set, then rerender all product item, which is bad things,
   * because we are nothing to change in product item.
   * Solution: use memoize callback hook which is 'useCallback'
   */
  const setProducTitle = title => {
    setTitle(title);
  };

  /**
   * useCallback(): it will either return an already stored function from the last render (if the dependencies haven’t changed),
   * or return the function you have passed during this render.
   *
   * Once use memoize callback function, click on product itme title,
   * it will set in header title without rerender product item.
   */
  const memoizeCallBack = useCallback(title => {
    setTitle(title);
  }, []);

  const multiplePrice = () => {
    return (price * 10).toFixed(2);
  };

  /**
   * useMemo(): return the result / value of the function
   * Once you use useMemo(), reduce the unnesessory rerndering. 
   */
  const memoizedPrice = useMemo(()=> multiplePrice(),[price]);


  return (
    <SafeAreaView style={styles.backgroundStyle}>
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={() => changeTitleColor()}
          style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text
            style={{
              marginHorizontal: 20,
              marginVertical: 10,
              color: titleColor ? 'orange' : 'cyan',
              fontSize: 20,
              fontWeight: 800,
            }}>
            {title}
          </Text>
          <Text
            style={{
              color: 'green',
              fontSize: 20,
              fontWeight: 800,
              marginHorizontal: 20,
              marginVertical: 10,
            }}>
            {memoizedPrice}
          </Text>
        </TouchableOpacity>

        <Text
          style={{
            marginHorizontal: 20,
            marginVertical: 10,
            color: 'grey',
            fontSize: 14,
          }}>
          (Tap on header, product title & price, to change header color, title and price multiplication Once you click, not re render product itme every time.)
        </Text>
        <FlatList
          horizontal={false}
          numColumns={2}
          style={{}}
          data={PRODUCT_JSON}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <ProductItem
              item={item}
              setTitle={
                memoizeCallBack
              } /*setProducTitle(title) //Normal approch */
              setPrice={setPrice}
            />
          )}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  backgroundStyle: {
    flex: 1,
    backgroundColor: 'black',
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
